<?php
namespace App\Http\Controllers\Solicitudescj;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use App\Mail\CreateUserStudent as CreateUserStudent;
use Yajra\Datatables\Datatables;
use Mail;

class EmployeeController extends Controller
{
	public function index(){
        /*$users=User::whereHas('roles', function ($query) {
		    $query->whereIn('abv',['SUP','TUT','SEC']);
		})->get();
		dd($users);*/
		//dd($users[0]->roles_label);
		//dd(User::all());
		return view('modules.Solicitudescj.employees.index');
	}

	public function create(){
		$roles = Role::whereIn('abv',['SUP','TUT','SEC'])->pluck('name','id');
		return view('modules.Solicitudescj.employees.create',compact('roles'));
	}

	public function getDatatable()
    {


	    $users=User::whereHas('roles', function ($query) {
		    $query->whereIn('abv',['SUP','TUT','SEC']);
		})->get();

    	//dd($postulants);

        return DataTables::of($users)->addColumn('actions', function ($select) {
			return '<p class="text-center"><a title="Gestionar Empleado" href="'.route('employees.show',$select->id).'"><span class="fa fa-cog"></span></a><p>';
        })->addColumn('roles_label', function ($select) {
            return $select->roles_label;
        })->addColumn('estado_label', function ($select) {
            return $select->estado_label;
        })->rawColumns(['actions','roles_label','estado_label'])
        ->make(true);


    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:mysql.users,email',
            'persona_id' => 'required|unique:mysql.users,persona_id',
            'roles' => 'required'
        ];
        $messages = [
            'name.required' => 'Escriba el nombre ',
            'email.required' => 'El Correo es requerido',
            'email.unique' => 'El correo ya se encuentra registrado',
            'persona_id.required' => 'La identificacion es requerida',
            'persona_id.unique' => 'La identificacion ya se encuentra registrada',

        ];
        $this->validate($request, $rules, $messages);

        $password=str_random(8);
        
        $user = new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password = bcrypt($password); 
        $user->persona_id = $request->persona_id;
        $user->estado = 'A';
        $user->save();

        $roles=$request->roles;
        
        $user->assignRole([$roles]);

        Mail::to($user->email)->send(new CreateUserStudent($user, $password));

        return redirect()->route('employees.index');
    }

    public function update(Request $request,$id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:mysql.users,email,'.$id.',id',
            'persona_id' => 'required|unique:mysql.users,persona_id,'.$id.',id',
            'roles' => 'required'
        ];
        $messages = [
            'name.required' => 'Escriba el nombre ',
            'email.required' => 'El Correo es requerido',
            'email.unique' => 'El correo ya se encuentra registrado',
            'persona_id.required' => 'La identificacion es requerida',
            'persona_id.unique' => 'La identificacion ya se encuentra registrada',

        ];
        $this->validate($request, $rules, $messages);

        
        $user = User::findOrFail($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->persona_id = $request->persona_id;
        $user->save();

        $roles=$request->roles;
        
        $user->syncRoles([$roles]);

        return redirect()->route('employees.index');
    }


    public function show($id){
     
        $employee=User::whereHas('roles', function ($query) {
		    $query->whereIn('abv',['SUP','TUT','SEC']);
		})->find($id);

		$roles = Role::whereIn('abv',['SUP','TUT','SEC'])->pluck('name','id');

        //dd($postulant);

        if(!$employee){
            return redirect()->route('postulant.index')->with('danger','No se ha encontrado el postulante');
        }  

        //dd($postulant);

        return view('modules.Solicitudescj.employees.show', compact('employee','roles'));
    }
}